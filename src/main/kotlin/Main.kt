import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.elasticsearch.action.bulk.BulkRequestBuilder
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder
import org.elasticsearch.transport.client.PreBuiltTransportClient
import java.io.File
import java.io.InputStreamReader
import java.net.InetAddress

/**
 * Created by Iamz on 1/19/17.
 * Copyright @ 2017 by Codetism Co., Ltd.
 */
fun main(args: Array<String>) {
    val startTime = System.nanoTime()

    val inputDirectory = File(args[0])
    if (!inputDirectory.exists() || !inputDirectory.isDirectory) {
        println("Wrong input directory.")
        return
    }

    val client = PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(
            InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300)
    )

    inputDirectory.listFiles().forEach { file ->
        processFile(file.absolutePath, client)
    }

    client.close()

    val endTime = System.nanoTime()
    println("Total time: ${(endTime - startTime) / 1e9}s")
}

fun processFile(inputFileName: String, client: TransportClient): Boolean {
    val startTime = System.nanoTime()

    val regex = Regex("\\d{8}")
    val dateString = regex.findAll(inputFileName).lastOrNull()?.value
    if (dateString == null) {
        println("Invalid date format.")
        return false
    }
    val year = dateString.substring(0..3)
    val month = dateString.substring(4..5)
    val day = dateString.substring(6..7)

    val inputFile = File(inputFileName)
    if (!inputFile.exists()) {
        println("No input file.")
        return false
    }

    InputStreamReader(inputFile.inputStream(), "x-windows-874").use { reader ->
        CSVParser(reader, CSVFormat.DEFAULT.withHeader()).use { parser ->
            var count = 0
            var bulkRequest: BulkRequestBuilder? = null
            for (record in parser) {
                if (count == 0) {
                    bulkRequest = client.prepareBulk()
                }
                bulkRequest?.add(client.prepareIndex("boonterm", "log")
                        .setSource(jsonBuilder()
                                .startObject()
                                .field("timestamp", "$year-$month-$day ${record.get("Timestamp")}")
                                .field("number", record.get("No"))
                                .field("topup_name", record.get("Topup Name"))
                                .field("mobile_number", record.get("Mobile Number"))
                                .field("publish_type", record.get("Publish Type"))
                                .field("location", record.get("Location"))
                                .field("region", record.get("Region"))
                                .field("province", record.get("Province"))
                                .field("amphur", record.get("Amphur"))
                                .field("tumbon", record.get("Tumbon"))
                                .field("campaign_name", record.get("Campaign Name"))
                                .endObject()))
                count++
                if (count == 500) {
                    count = 0
                    val bulkResponse = bulkRequest?.get()
                    if (bulkResponse?.hasFailures() ?: true) {
                        println("Indexing failed!!!")
                        return false
                    }
                }
            }
        }
    }

    val stopTime = System.nanoTime()
    println("Processed $inputFileName in ${(stopTime - startTime) / 1e9}s")

    return true
}